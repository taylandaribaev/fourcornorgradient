import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.12

Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    property var initColors: [
        "blue",
        "black",
        "green",
        "red"
    ]

    ColorDialog {
        id: colorDialog

        property var rect

        visible: false
        title: "Please choose a color"

        onAccepted: rect.color = colorDialog.color;
    }

    function pickColor(rect) {
        if (rect) {
            colorDialog.rect = rect;
            colorDialog.visible = true;
        }
    }

    Rectangle {
        anchors {
            fill: parent
            topMargin: 20
            leftMargin: 20
            rightMargin: 20
            bottomMargin: 50
        }

        ShaderEffect {
            id: shaderEffect

            property color color0: initColors[0]
            property color color1: initColors[1]
            property color color2: initColors[2]
            property color color3: initColors[3]

            anchors.fill: parent

            fragmentShader: "
                varying mediump vec2 qt_TexCoord0;

                uniform lowp vec4 color0;
                uniform lowp vec4 color1;
                uniform lowp vec4 color2;
                uniform lowp vec4 color3;

                vec4 lerp(vec4 a, vec4 b, float w)
                {
                    return a + w*(b-a);
                }

                void main() {
                    gl_FragColor = lerp(lerp(color0, color1, qt_TexCoord0.y),  lerp(color2, color3, qt_TexCoord0.y), qt_TexCoord0.x);
                }
            "
        }
    }

    Row {
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
            bottomMargin: 20
        }
        spacing: 30

        Repeater {
            id: repeater

            model: 4

            Rectangle {
                id: delegate

                width: 100
                height: 20
                color: window.initColors[index]

                Text {
                    anchors.centerIn: parent
                    font.pixelSize: 14
                    text: parent.color
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        window.pickColor(parent);
                    }
                }

                Component.onCompleted: {
                    shaderEffect["color" + index] = Qt.binding(function() { return color; })
                }
            }
        }
    }
}
